# plugin

## How to use
##### 1. You need to load 2 files on your websites.
##### 2. First file is JS file.

```html
<script src="path-to-plugin-folder/plugin.js"></script>
```

##### 3. Second file is CSS file.

```html
<link href="path-to-plugin-folder/plugin.css" rel="stylesheet">
```

##### 4. After load plugin JS, you can use this functionality.
```js
const pluginOptions = {}; // see options on next chapter

window.PLUGIN_API.init(pluginOptions);
```

## Plugin Options Interface

```typescript
interface PluginStep {
  // type of photo ('damage' | 'vehicle' or ets.)
  photoType: string;
  // show retake page after capture (default is true)
  withRetake?: boolean;
  // embed geolocation, date, time in the photo (default is true)
  embedGeo?: boolean;
  // show step in landscape mode (default is false it is mean forcePortrait)
  forceLandscape?: boolean;
  // show comment block after make photo
  isShowComment?: boolean;
  // image for help photo modal (if empty "Help modal" doesnt show before capture)
  helpImg?: string;
  // title below help image in modal
  helpTitle?: string;
  // description of help image
  helpDescription?: string;
  // image for mask over video
  maskImg?: string;
  // small image in slider during photo
  previewImg?: string;
  // title for preview image in slider during photo
  previewTitle?: string;
}

interface PluginProps {
  // selector to element where plugin will render
  selector: string;
  steps: PluginStep[];
  // show summary page (default 'true') 
  // if false completeFormCallback invoke in last capture photo step
  withSummary?: boolean;
  completeFormCallback: (form: ResultForm) => void;
  // default true
  disableStreamAfterComplete?: boolean;
  translates?: {
    enablePermissionsTitle?: string;
    enablePermissionsDescription?: string;
    enablePermissionsBtn?: string;
    refreshPageBtn?: string;
    disablePermissionsTitle?: string;
    enablePermissionsSubTitle?: string;
    disablePermissionsSubTitle?: string;
    enableLocationBtn?: string;
    rejectCameraDescription?: string;
    rejectLocationDescription?: string;
    // special text message for Safari
    rejectLocationDescriptionSafari?: string;
    enablePermissionsSkip?: string;
    tapToDismiss?: string;
    commentFieldPlaceholder?: string;
    positionInLandscape?: string;
    positionInPortrait?: string;
    reviewMediaTitle?: string;
    submitBtn?: string;
    additional?: string;
  };
  icons?: {
    permissions?: string;
    rejectPermissions?: string;
    photoBtn?: string;
    retake?: string;
    usePhoto?: string;
    additional?: string;
    spinner?: string;
  };
  gpsInfo?: {
    // default 'top-right'
    position?: 'top-left' | 'top-right' | 'bottom-left' | 'bottom-right' 
                          | 'center-top' | 'center-bottom';
    styles?: {
      // only words or rgb (e.g 'white' or 'rgb(255, 255, 10)')
      fontColor?: string;
      // only px (default 25px)
      fontSize?: string;
      // default Arial
      fontFamily?: string;
      // default black
      backgroundColor?: string;
      // only px only 4 values supports (e.g '10px 10px 10px 10px') only integer
      padding?: string;
      // only px
      borderRadius?: string;
    };
  };
}
```

### Example for "One Shot"


```html
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- load plugin style -->
    <link href="plugin.css" rel="stylesheet">
</head>
<body>
<!-- this place where plugin will render -->
<div class="plugin"></div>

<!-- load plugin JS synchronously -->
<script src="plugin.js"></script>

<!-- init plugin options on your JS file or inline JS like this -->
<script>
  window.PLUGIN_API.init({
    selector: '.plugin',
    completeFormCallback: form => {
      // make what your want :)
      console.log(form);
    },
    withSummary: false,
    steps: [
      {
        withRetake: false,
        maskImg: 'path-to-mask-img',
        previewImg: 'path-to-preview-img',
        previewTitle: 'Driver Side Front',
        helpImg: 'path-to-help-img',
        helpTitle: 'Driver Side Front',
        helpDescription: 'Fit your vehicle into the outline as shown in the example above. Then capture the photo.',
      },
    ],
  });
</script>
</body>
</html>
```

### Full example:


```html
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- load plugin style -->
    <link href="plugin.css" rel="stylesheet">
</head>
<body>
<!-- this place where plugin will render -->
<div class="plugin"></div>

<!-- load plugin JS synchronously -->
<script src="plugin.js"></script>

<!-- init plugin options on your JS file or inline JS like this -->
<script>
  window.PLUGIN_API.init({
    selector: '.plugin',
    completeFormCallback: form => {
      // make what your want :)
      console.log(form);
    },
    steps: [
      {
        photoType: 'vehicle',
        maskImg: 'path-to-mask-img',
        previewImg: 'path-to-preview-img',
        previewTitle: 'Driver Side Front',
        helpImg: 'path-to-help-img',
        helpTitle: 'Driver Side Front',
        helpDescription: 'Fit your vehicle into the outline as shown in the example above. Then capture the photo.',
      },
      {
        photoType: 'vehicle',
        maskImg: 'path-to-mask-img',
        previewImg: 'path-to-preview-img',
        previewTitle: 'Driver Side Rear',
        helpImg: 'path-to-help-img',
        helpTitle: 'Driver Side Rear',
        embedGeo: false
      },
      {
        photoType: 'damage',
        forceLandscape: true,
        isShowComment: false,
        maskImg: 'path-to-mask-img',
        previewImg: 'path-to-preview-img',
        previewTitle: 'Damage - Right',
        helpImg: 'path-to-help-img',
        helpTitle: 'Damage - Right',
      },
      {
        photoType: 'damage',
        isShowComment: false,
        maskImg: 'path-to-mask-img',
        previewImg: 'path-to-preview-img',
        previewTitle: 'Damage - Left',
        helpImg: 'path-to-help-img',
        helpTitle: 'Damage - Left',
      },
    ],
  });
</script>
</body>
</html>
```

### Result Form Interface

```typescript
interface FormStep {
  // base64 string
  photo: string;
  photoType: PluginStep['photoType'];
  comment?: string;
  geolocation?: {
    latitude?: number;
    longitude?: number;
    date: string;
    time: string;
  };
}

interface ResultForm {
  steps: FormStep[];
  // additional photo
  additionalSteps?: string[];
}
```

