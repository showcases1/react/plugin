import { resolve } from 'path';

export default {
  /**
   * Function that mutates the original webpack config.
   * Supports asynchronous changes when a promise is returned (or it's an async function).
   *
   * @param {object} config - original webpack config.
   * @param {object} env - options passed to the CLI.
   * @param {WebpackConfigHelpers} helpers - object with useful helpers for working with the webpack config.
   * @param {object} options - this is mainly relevant for plugins (will always be empty in the config), default to an empty object
   **/
  webpack(config, env, helpers, options) {
    helpers.getLoadersByName(config, 'css-loader').forEach(({ loader }) => {
      loader.options = Object.assign(loader.options, {
        modules: true,
        localIdentName: '[local]__[hash:base64:5]',
        importLoaders: 2,
        camelCase: true,
        namedExport: true,
        silent: true,
      });
    });

    // Use any `index` file, not just index.js
    config.resolve.alias['preact-cli-entrypoint'] = resolve(process.cwd(), 'src', 'index');

    config.resolve.alias['src'] = resolve(__dirname, 'src');

    // add preact/compat mapper
    config.resolve.alias['react'] = 'preact/compat';
    config.resolve.alias['react-dom'] = 'preact/compat';
  },
};
