# plugin

## CLI Commands
*   `npm install`: Installs dependencies

*   `npm run start`: Runs `serve` or `dev`, depending on `NODE_ENV` value. Defaults to `dev server`

*   `npm run dev`: Run a development, HMR server

*   `npm run serve`: Run a production-like server

*   `npm run build`: Production-ready build

*   `npm run lint`: Pass TypeScript files using TSLint

*   `npm run test`: Run Jest and [`preact-render-spy`](https://github.com/mzgoddard/preact-render-spy) for your tests


## Build
* npm install
* npm run build:simple
* go to folder build
* take 2 files bundle.[hash].js and bundle.[hash].css
* insert this file into site
* PluginApi.init(config) is available in JS code

## Config API

For detailed explanation on how things work, checkout the [CLI Readme](https://github.com/developit/preact-cli/blob/master/README.md).

## Convert to PDF
https://md2pdf.netlify.com/
