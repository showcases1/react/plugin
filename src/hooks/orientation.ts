import { useEffect, useState } from 'preact/hooks';
import { isSafariOniOS } from '../utils/mobile';

const isLandscapeFn = () => (isSafariOniOS ? window.matchMedia('(orientation: landscape)').matches : screen.orientation.angle === 90);

/**
 * Hook to get orientation
 */
export const useOrientation = () => {
  const [isLandscape, setLandscape] = useState(isLandscapeFn());
  const [mounted, setMounted] = useState(true);

  useEffect(() => {
    // detect orientation change
    const handleOrientation = () =>
      setTimeout(() => {
        if (mounted) {
          setLandscape(isLandscapeFn());
        }
      }, 100);

    window.addEventListener('orientationchange', handleOrientation);

    return () => {
      window.removeEventListener('orientationchange', handleOrientation);

      setMounted(false);
    };
  }, []);

  return {
    isLandscape,
  };
};
