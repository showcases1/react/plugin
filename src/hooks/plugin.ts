import { useCallback, useState } from 'preact/hooks';
import { PluginProps } from '../types';

/**
 * Hoot to get callback to possibility invoke only once
 */
export const useCompleteFormCallback = (callback: PluginProps['completeFormCallback']) => {
  const [invoke, setInvoke] = useState(false);

  return useCallback(
    (...args: Parameters<PluginProps['completeFormCallback']>) => {
      if (!invoke) {
        setInvoke(true);

        callback(...args);
      }
    },
    [invoke, setInvoke, callback]
  );
};
