import { useOrientation } from './orientation';
import { getLandscapeVideoContainerSize, getPortraitVideoContainerSize } from '../utils/mediaDevices';

/**
 * Hook to get container size for video and image
 */
export const useMediaContainerSize = () => {
  const { isLandscape } = useOrientation();

  return isLandscape ? getLandscapeVideoContainerSize() : getPortraitVideoContainerSize();
};
