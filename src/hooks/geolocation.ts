import { useEffect, useState } from 'preact/hooks';
import { getGeolocation } from '../utils/geolocation';

/**
 * Hook to get geolocation invoke once when component mounted
 */
export const useGeolocation = (...deps: any[]) => {
  const [position, setPosition] = useState<Position | null>(null);

  useEffect(() => {
    const getPosition = async () => {
      let position = null;

      try {
        position = await getGeolocation();
      } catch {}

      setPosition(position);
    };

    getPosition();
  }, [...deps]);

  return position;
};
