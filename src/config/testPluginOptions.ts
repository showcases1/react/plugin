/**
 * File for create test data to test plugin steps
 */
import Mask1 from '../assets/cam_overlay_sedan_df.svg';
import Preview1 from '../assets/carousel_vehicle_sedan_df.svg';
import Help1 from '../assets/help_overlay_sedan_df.svg';
import Mask2 from '../assets/cam_overlay_sedan_dr.svg';
import Preview2 from '../assets/carousel_vehicle_sedan_dr.svg';
import Help2 from '../assets/help_overlay_sedan_dr.svg';
import Mask3 from '../assets/cam_overlay_sedan_pf.svg';
import Preview3 from '../assets/carousel_vehicle_sedan_pf.svg';
import Help3 from '../assets/help_overlay_sedan_pf.svg';
import Mask4 from '../assets/cam_overlay_sedan_pr.svg';
import Preview4 from '../assets/carousel_vehicle_sedan_pr.svg';
import Help4 from '../assets/help_overlay_sedan_pr.svg';
import DamageMask from '../assets/damage_right_co.svg';
import DamagePreview from '../assets/damage_right_carousel.svg';
import DamageHelp from '../assets/damage_right_ho.svg';
import DamageMask1 from '../assets/damage_left_co.svg';
import DamageHelp1 from '../assets/damage_left_ho.svg';
import { PluginProps, ResultForm } from '../types';

export const getTestOptions = (): PluginProps => {
  const options = {
    selector: 'test',
    withSummary: false,
    completeFormCallback: (form: ResultForm) => {
      console.log('form', form);
    },
    // gpsInfo: {
    //   position: 'center-top',
    //   styles: {
    //     fontSize: '40px',
    //     fontColor: 'green',
    //     backgroundColor: 'yellow',
    //     padding: '15px 15px 15px 15px',
    //     borderRadius: '0',
    //   },
    // },
    steps: [
      {
        //photoType: 'vehicle',
        withRetake: false,
        isShowComment: false,
        maskImg: Mask1,
        //previewImg: '',
        //previewTitle: '',
        //helpImg:'',
        //helpTitle: '',
        //helpDescription: '',
        embedGeo: false,
      },
      // {
      //   photoType: 'Drow',
      //   maskImg: Mask2,
      //   previewImg: Preview2,
      //   withRetake: true,
      //   previewTitle: 'Driver Side Rear',
      //   helpImg: Help2,
      //   forceLandscape: false,
      //   helpTitle: 'Driver Side Rear',
      //   embedGeo: false,
      // },
      // {
      //   photoType: 'vehiclE',
      //   maskImg: Mask3,
      //   withRetake: false,
      //   previewImg: Preview3,
      //   previewTitle: 'Passenger Side Rear',
      //   helpImg: Help3,
      //   helpTitle: 'Passenger Side Rear',
      // },
      // {
      //   photoType: 'House',
      //   maskImg: Mask4,
      //   previewImg: Preview4,
      //   previewTitle: 'Passenger Side Front',
      //   helpImg: Help4,
      //   helpTitle: 'Passenger Side Front',
      // },
      // {
      //   photoType: 'VehiclE',
      //   forceLandscape: true,
      //   isShowComment: false,
      //   maskImg: DamageMask,
      //   previewImg: DamagePreview,
      //   previewTitle: 'Damage - Right',
      //   helpImg: DamageHelp,
      //   helpTitle: 'Damage - Right',
      // },
      // {
      //   photoType: 'damage',
      //   isShowComment: false,
      //   maskImg: DamageMask1,
      //   previewImg: DamagePreview,
      //   previewTitle: 'Damage - Left',
      //   helpImg: DamageHelp1,
      //   helpTitle: 'Damage - Left',
      //   embedGeo: false,
      // },
    ],
  } as PluginProps;

  window.PLUGIN_API.__options = options;

  return options;
};
