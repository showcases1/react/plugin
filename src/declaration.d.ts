import { JSX } from 'preact';
import { PluginProps } from './types';

export = JSX;
export as namespace JSX;

declare global {
  interface Window {
    PLUGIN_API: {
      init: (options: PluginProps) => void;
      __camera?: MediaTrackSettings;
      __options?: PluginProps;
    };
  }
}
