import './style/index.css';
import { h } from 'preact';
import Plugin from './components/app';
import { render } from 'preact';
import { PluginProps } from './types';

if ((module as any).hot) {
  require('preact/debug');
}

// Set Up Plugin
const PLUGIN_API = {
  init: (options: PluginProps) => {
    const renderElement = document.querySelector(options.selector);

    if (renderElement) {
      render(<Plugin {...options} />, renderElement);
    }

    window.PLUGIN_API.__options = options;
  },
};

window.PLUGIN_API = PLUGIN_API;

export default Plugin;
