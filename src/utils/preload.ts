/**
 * Preload all plugin images when start
 */

const preloadImage = (images: (string | undefined)[]) => {
  images.forEach(src => {
    if (!src) return;

    const img = new Image();

    img.src = src;
  });
};

export const preload = (images: (string | undefined)[][] | undefined) => {
  if (!Array.isArray(images)) return;

  images.forEach(images => preloadImage(images));
};
