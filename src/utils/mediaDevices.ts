import { getGeolocationWithDate } from './geolocation';
import { drawGps } from './canvas';
import { isDisableStream } from './plugin';

/**
 * Polyfill for mediaDevices.getUserMedia
 */
(function() {
  // @ts-ignore
  if (!navigator.mediaDevices) navigator.mediaDevices = {};
  if (!navigator.mediaDevices.getUserMedia) {
    // @ts-ignore
    const getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.getUserMedia;

    if (getUserMedia) {
      navigator.mediaDevices.getUserMedia = constraints => {
        return new Promise((resolve, reject) => {
          getUserMedia(
            constraints,
            (stream: MediaStream) => {
              resolve(stream);
            },
            (error: any) => {
              reject(error);
            }
          );
        });
      };
    } else {
      navigator.mediaDevices.getUserMedia = () => {
        return new Promise((resolve, reject) => {
          reject('getUserMedia is not supported in this browser.');
          alert('WebRTC is not supported in this browser.');
        });
      };
    }
  }
})();

/**
 * Media query constraints for video
 */
const constraints: MediaStreamConstraints = {
  video: {
    facingMode: 'environment',
    aspectRatio: 4 / 3,
    // height: 1200,
  },
  audio: false,
};

/**
 * Get media support needs HTTPS
 */
export const getUserMedia = (): Promise<void | MediaStream> => {
  if (navigator.mediaDevices) {
    return navigator.mediaDevices.getUserMedia(constraints);
  }

  return Promise.reject();
};

/**
 * Stop media stream
 */
export const turnOffUserMedia = (stream: MediaStream | null) => {
  const isDisable = isDisableStream();

  if (stream && isDisable) {
    stream.getTracks().forEach(track => track.stop());
  }
};

/**
 * Check media device accepted
 */
export const isMediaDeviceAccepted = () => {
  return new Promise(async (res, rej) => {
    let videoDevice;

    try {
      if (typeof navigator?.mediaDevices?.enumerateDevices !== 'function') return rej();

      const devices = await navigator.mediaDevices.enumerateDevices();

      videoDevice = devices.find(device => device.kind === 'videoinput' && device.deviceId);
    } catch {
      return rej();
    }

    videoDevice ? res() : rej();
  });
};

/**
 * Get video stream and apply to video element
 */
export const acceptWebCamToVideo = async (video?: HTMLVideoElement): Promise<MediaStream | null> => {
  if (!video) return null;

  const stream = await getUserMedia();

  if (!stream) return null;

  video.srcObject = stream;

  video.play();

  // Hacks for Mobile Safari
  video.setAttribute('playsinline', 'true');

  return stream;
};

export const getCanvasImage = (canvas: HTMLCanvasElement) => canvas.toDataURL('image/jpeg');

/**
 * Capture screen from video with coords and return base64
 */
export const captureVideo = async (video?: HTMLVideoElement, embedGeo = true): Promise<string> => {
  if (!video) return '';

  // check enable geo
  let geo = null;
  try {
    geo = await getGeolocationWithDate();
  } catch {}

  const canvas = document.createElement('canvas');

  const imageWidth = 1600;
  const imageHeight = 1200;

  canvas.width = imageWidth;
  canvas.height = imageHeight;

  const ctx = canvas.getContext('2d');

  if (!ctx) return '';

  // crop the image, cuz portrait video has inverted size
  const videoWidth = video.videoWidth;
  const aspectRatio = 4 / 3;
  const cropImageHeight = videoWidth / aspectRatio;

  ctx.drawImage(video, 0, 0, videoWidth, cropImageHeight, 0, 0, imageWidth, imageHeight);

  // write geolocation info to photo
  if (geo && embedGeo) {
    drawGps(canvas, geo);
  }

  return getCanvasImage(canvas);
};

/**
 * Get special size for container in portrait mode to correct work on mobile phone
 */
export const getPortraitVideoContainerSize = () => {
  // @ts-ignore
  // if (window.PLUGIN_API?.__camera.height && window.PLUGIN_API?.__camera.width) {
  //   return {
  //     width: `${screen.width}px`,
  //     height: `${screen.width * (window.PLUGIN_API?.__camera.height / window.PLUGIN_API?.__camera.width)}px`,
  //   };
  // }

  return {
    width: `${screen.width}px`,
    height: `${screen.width * 0.75}px`,
  };
};

/**
 * Get special size for container in landscape mode to correct work on mobile phone
 */
export const getLandscapeVideoContainerSize = () => {
  return {
    width: `${window.innerHeight / 0.75}px`,
    height: `${window.innerHeight}px`,
  };
};
