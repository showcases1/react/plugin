/**
 * Util to detect mobile device
 */
export const isMobileDevice = () => {
  return typeof window.orientation !== 'undefined' || navigator.userAgent.indexOf('IEMobile') !== -1;
};

export const isSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);
const iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

/**
 * Check Safari and iOS
 */
export const isSafariOniOS = isSafari && iOS;
