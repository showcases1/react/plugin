import styles from './Comment.module.scss';
import { FunctionalComponent as FC, h } from 'preact';
import { useCallback, useState } from 'preact/hooks';
import { useOrientation } from '../../hooks/orientation';
import { getTranslates } from '../../utils/plugin';

const CLASS = 'comment';

interface Props {
  onChange: (value: string) => void;
  value: string;
  showComment: boolean;
}

const MAX_COMMENT_SYMBOL = 250;

/**
 * Component with comment field and oval
 */
export const Comment: FC<Props> = ({ children, onChange, value: valueProps = '', showComment = true }) => {
  const { isLandscape } = useOrientation();
  const [comment, setComment] = useState({ value: valueProps });
  const translates = getTranslates();

  const handleComment = useCallback(
    ({ currentTarget: { value } }: h.JSX.TargetedEvent<HTMLTextAreaElement>) => {
      if (comment.value.length < MAX_COMMENT_SYMBOL) {
        setComment({ value });

        onChange(value);
      } else {
        // special behavior for preact
        setComment({ value: value.substring(0, value.length - 1) });
      }
    },
    [onChange, comment]
  );

  return (
    <div className={styles[`${CLASS}__content-wrapper`]}>
      {children}

      <div className={styles[`${CLASS}__content`]}>
        {showComment && (
          <div className={styles[`${CLASS}__comment-wrapper`]}>
            <textarea
              style={{ minHeight: isLandscape ? '0' : '150px' }}
              value={comment.value}
              className={styles[`${CLASS}__comment-area`]}
              placeholder={translates.commentFieldPlaceholder}
              onChange={handleComment}
            />
          </div>
        )}
      </div>

      <div className={styles[`${CLASS}__oval-wrapper`]}>
        <div className={styles[`${CLASS}__oval`]} />
      </div>
    </div>
  );
};
