import { FunctionalComponent as FC, h } from 'preact';
import { useCallback, useContext, useState } from 'preact/hooks';
import { CommentPhoto } from '../PhotoStepsPage/CommentPhoto';
import { Photo } from '../PhotoStepsPage/Photo';
import { PluginStep } from '../../types';
import { FormContext, actions } from '../../reducers/form';

const { setRetakePhoto } = actions;

interface Props {
  steps: PluginStep[];
}

enum LocalSteps {
  MakePhoto,
  CommentPhoto,
}

export const RetakePhotoStep: FC<Props> = ({ steps }) => {
  const {
    state: { resultForm, retakeIndex },
    dispatch,
  } = useContext(FormContext);
  const step = steps[retakeIndex];
  const formStep = resultForm.steps[retakeIndex];
  const [photo, setPhoto] = useState(formStep.photo);
  const [photoState, setPhotoState] = useState<LocalSteps>(LocalSteps.CommentPhoto);

  const handlePhoto = useCallback(
    (newPhoto: string) => {
      setPhotoState(LocalSteps.CommentPhoto);

      setPhoto(newPhoto);
    },
    [photo]
  );

  const handleRetake = useCallback(() => {
    setPhotoState(LocalSteps.MakePhoto);
  }, []);

  const handleSave = useCallback(
    (comment: string) => {
      dispatch(setRetakePhoto(photo, comment));
    },
    [photo]
  );

  return photoState === LocalSteps.MakePhoto ? (
    <Photo forceLandscape={step.forceLandscape} maskImg={step.maskImg} embedGeo={step.embedGeo} onPhoto={handlePhoto} />
  ) : (
    <CommentPhoto
      forceLandscape={step.forceLandscape}
      comment={formStep.comment}
      showComment={step.isShowComment}
      onSave={handleSave}
      onRetake={handleRetake}
      img={photo}
    />
  );
};
