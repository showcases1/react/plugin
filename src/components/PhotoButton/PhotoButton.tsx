import { FunctionalComponent as FC, h } from 'preact';
import styles from './PhotoButton.module.scss';
import { getIcons } from '../../utils/plugin';

const CLASS = 'photo-button';

interface Props {
  onClick: h.JSX.MouseEventHandler<HTMLDivElement>;
}

export const PhotoButton: FC<Props> = ({ onClick }) => {
  const icons = getIcons();

  return <div style={{ backgroundImage: `url(${icons.photoBtn})` }} className={styles[CLASS]} onClick={onClick} />;
};
