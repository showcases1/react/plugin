import { FunctionalComponent as FC, h } from 'preact';
import styles from './Separator.module.scss';

const CLASS = 'separator';

interface Props {
  width?: string | number;
  height?: string | number;
  fit?: boolean;
}

export const Separator: FC<Props> = ({ width = 0, height = '15px', fit = false }) => (
  <div className={styles[CLASS]} style={{ width, height, flex: fit ? '1 1 auto' : 'none' }} />
);
