import { h } from 'preact';
import { useCallback, useContext, useState } from 'preact/hooks';
import { CommentPhoto } from '../PhotoStepsPage/CommentPhoto';
import { Photo } from '../PhotoStepsPage/Photo';
import { actions, FormContext } from '../../reducers/form';

const { setAdditionalStep } = actions;

enum AdditionalSteps {
  MakePhoto,
  CommentPhoto,
}

export const AdditionalPhotoStep = () => {
  const [photo, setPhoto] = useState('');
  const [photoStep, setPhotoStep] = useState<AdditionalSteps>(AdditionalSteps.MakePhoto);
  const { dispatch } = useContext(FormContext);

  const handlePhoto = useCallback(
    (newPhoto: string) => {
      setPhotoStep(AdditionalSteps.CommentPhoto);

      setPhoto(newPhoto);
    },
    [photo]
  );

  const handleRetake = useCallback(() => {
    setPhotoStep(AdditionalSteps.MakePhoto);
  }, []);

  const handleSave = useCallback(() => {
    dispatch(setAdditionalStep(photo));
  }, [photo]);

  return photoStep === AdditionalSteps.MakePhoto ? (
    <Photo showComment={false} embedGeo={false} onPhoto={handlePhoto} />
  ) : (
    <CommentPhoto showComment={false} onSave={handleSave} onRetake={handleRetake} img={photo} />
  );
};
