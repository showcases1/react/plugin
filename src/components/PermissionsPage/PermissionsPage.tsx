import { h } from 'preact';
import styles from './PermissionsPage.module.scss';
import { Layout } from '../Layout';
import { useCallback, useContext, useRef, useState } from 'preact/hooks';
import { getUserMedia } from '../../utils/mediaDevices';
import { Button } from '../Button';
import { Spinner } from '../Spinner';
import { GEOLOCATION_ENABLE, GEOLOCATION_KEY, getGeolocation } from '../../utils/geolocation';
import { getIcons, getTranslates } from '../../utils/plugin';
import { delay } from '../../utils/common';
import { FormContext, actions } from '../../reducers/form';
import { isSafari } from '../../utils/mobile';

const { changeStep } = actions;

const CLASS = 'permissions';

export const PermissionsPage = () => {
  const [rejectCamera, setRejectCamera] = useState(false);
  const [rejectGeo, setRejectGeo] = useState(false);
  const [loading, setLoading] = useState(false);
  const videoRef = useRef<{ base: HTMLVideoElement }>();
  const translates = getTranslates();
  const icons = getIcons();
  const { dispatch } = useContext(FormContext);

  const handlePermissions = useCallback(() => {
    if (rejectCamera) return window.location.reload();

    setLoading(true);

    delay()
      .then(getUserMedia)
      .then(result => {
        // save camera settings
        if (result instanceof MediaStream) {
          window.PLUGIN_API.__camera = result.getTracks()[0]?.getSettings();
          // alert(JSON.stringify(window.PLUGIN_API.camera));
        }
      })
      .catch(() => {
        setRejectCamera(true);
        // break promise chain
        throw new Error();
      })
      .then(() =>
        getGeolocation()
          .then(() => {
            localStorage.setItem(GEOLOCATION_KEY, GEOLOCATION_ENABLE);
          })
          .catch(() => {
            setRejectGeo(true);
            // break promise chain
            throw new Error();
          })
      )
      .then(() => {
        dispatch(changeStep());
        setLoading(false);
      })
      .catch(() => setLoading(false));
  }, [rejectCamera, videoRef]);

  const handleGeo = useCallback(() => {
    setLoading(true);

    delay()
      .then(getGeolocation)
      .then(() => dispatch(changeStep()))
      .catch(() => {
        setRejectGeo(true);
      })
      .then(() => setLoading(false));
  }, []);

  return (
    <Layout>
      <div className={styles[CLASS]}>
        {loading && <Spinner />}

        <div className={styles[`${CLASS}__content`]}>
          <p className={styles[`${CLASS}__title`]}>{rejectGeo ? translates.disablePermissionsTitle : translates.enablePermissionsTitle}</p>
          {!rejectCamera && !rejectGeo ? (
            <img alt="" src={icons.permissions} />
          ) : (
            rejectCamera && <img alt="" src={icons.rejectPermissions} />
          )}
          <p className={styles[`${CLASS}__perm`]}>
            {!rejectCamera && !rejectGeo ? translates.enablePermissionsSubTitle : rejectCamera && translates.disablePermissionsSubTitle}
          </p>
          <p className={styles[`${CLASS}__description`]}>
            {rejectCamera
              ? translates.rejectCameraDescription
              : rejectGeo
              ? isSafari
                ? translates.rejectLocationDescriptionSafari
                : translates.rejectLocationDescription
              : translates.enablePermissionsDescription}
          </p>
        </div>

        <div className={styles[`${CLASS}__btn`]}>
          {!rejectGeo && (
            <Button onClick={handlePermissions}>{rejectCamera ? translates.refreshPageBtn : translates.enablePermissionsBtn}</Button>
          )}
          {rejectGeo && <Button onClick={handleGeo}>{translates.enableLocationBtn}</Button>}
          {rejectGeo && (
            <p className={styles[`${CLASS}__skip`]} onClick={() => dispatch(changeStep())}>
              {translates.enablePermissionsSkip}
            </p>
          )}
        </div>
      </div>
    </Layout>
  );
};
