import { FunctionalComponent as FC, h } from 'preact';
import styles from './Slider.module.scss';
import cn from 'classnames';

const CLASS = 'slider';

export interface SliderProps {
  // current image, base64 or link
  current?: string;
  // title for current image
  currentTitle?: string;
  currentCount: number;
  // slider count
  count: number;
  // current image, base64 or link
  before?: string;
  // current image, base64 or link
  after?: string;
}

export const Slider: FC<SliderProps> = ({ current, currentTitle, before, after, count = 0, currentCount = 0 }) => (
  <div className={styles[CLASS]}>
    <div className={cn(styles[`${CLASS}__item`], styles[`${CLASS}__before`])}>
      {currentCount !== 0 && before && (
        <div className={cn(styles[`${CLASS}__img`], styles[`${CLASS}__img--small`])} style={{ backgroundImage: `url(${before})` }} />
      )}
    </div>
    <div className={cn(styles[`${CLASS}__item`], styles[`${CLASS}__current`])}>
      {current && (
        <div className={cn(styles[`${CLASS}__img`], styles[`${CLASS}__img--big`])} style={{ backgroundImage: `url(${current})` }} />
      )}
      <span>{currentTitle}</span>
    </div>
    <div className={cn(styles[`${CLASS}__item`], styles[`${CLASS}__after`])}>
      {after && (
        <div className={cn(styles[`${CLASS}__img`], styles[`${CLASS}__img--small`])} style={{ backgroundImage: `url(${after})` }} />
      )}
      <span>
        {currentCount + 1} of {count + 1}
      </span>
    </div>
  </div>
);
