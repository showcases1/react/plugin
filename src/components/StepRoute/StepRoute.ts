import { VNode } from 'preact';
import { Steps } from '../../types';
import { useContext } from 'preact/hooks';
import { FormContext } from '../../reducers/form';

interface Props {
  step: Steps;
  children: VNode<any>;
}

/**
 * Component for render children if step the same
 */
export const StepRoute = ({ step, children }: Props) => {
  const {
    state: { step: currentStep },
  } = useContext(FormContext);

  if (currentStep !== step) return null;

  return children;
};
