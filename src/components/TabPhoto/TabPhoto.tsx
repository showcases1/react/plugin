import { FunctionalComponent as FC, h } from 'preact';
import styles from './TabPhoto.module.scss';
import { FormStep, PluginProps, ResultForm, Steps } from '../../types';
import { getIcons } from '../../utils/plugin';
import { useContext } from 'preact/hooks';
import { FormContext, actions } from '../../reducers/form';

const { changeStep, retakePhoto } = actions;

const CLASS = 'tab-photo';

interface Props {
  title: string;
  steps: PluginProps['steps'];
  formSteps: ResultForm['steps'];
  filterKey: FormStep['photoType'];
}

interface AdditionalProps {
  title: string;
}

interface PhotoProps {
  photo: string;
  title?: string;
  onClick?: () => void;
}

interface PhotoItemProps {
  title?: string;
  onClick?: () => void;
}

interface TabPhotoPanel {
  title: string;
}

const PhotoItem: FC<PhotoItemProps> = ({ children, title, onClick }) => (
  <div onClick={onClick} className={styles[`${CLASS}__photo`]}>
    <div className={styles[`${CLASS}__photo-wrapper`]}>{children}</div>
    <p className={styles[`${CLASS}__photo-title`]}>{title}</p>
  </div>
);

const Photo: FC<PhotoProps> = ({ photo, title, onClick }) => (
  <PhotoItem title={title} onClick={onClick}>
    <img className={styles[`${CLASS}__photo-img`]} src={photo} alt="" />
  </PhotoItem>
);

const TabPhotoPanel: FC<TabPhotoPanel> = ({ children, title }) => (
  <div className={styles[CLASS]}>
    <p className={styles[`${CLASS}__title`]}>{title}</p>
    <div className={styles[`${CLASS}__photos`]}>{children}</div>
  </div>
);

export const TabPhotoAdditional: FC<AdditionalProps> = ({ title }) => {
  const { additional } = getIcons();
  const {
    state: {
      resultForm: { additionalSteps = [] },
    },
    dispatch,
  } = useContext(FormContext);

  return (
    <TabPhotoPanel title={title}>
      {additionalSteps.map((photo, idx) => (
        <Photo key={idx} photo={photo} title={`Photo ${idx + 1}`} />
      ))}
      <PhotoItem title={`Photo ${additionalSteps.length + 1}`} onClick={() => dispatch(changeStep(Steps.Additional))}>
        <div className={styles[`${CLASS}__photo-img`]} style={{ backgroundImage: `url(${additional})` }} />
      </PhotoItem>
    </TabPhotoPanel>
  );
};

export const TabPhoto: FC<Props> = ({ title, steps, formSteps, filterKey }) => {
  const { dispatch } = useContext(FormContext);
  const items = formSteps
    .map((step, idx) => ({
      ...step,
      formIndex: idx,
    }))
    .filter(step => step.photoType.toLowerCase() === filterKey.toLowerCase());
  const filteredSteps = steps.filter(step => step.photoType.toLowerCase() === filterKey.toLowerCase());

  if (items.length === 0) return null;

  const titleParsed =
    title
      .toLowerCase()
      .substring(0, 1)
      .toUpperCase() + title.toLowerCase().substring(1);

  return (
    <TabPhotoPanel title={titleParsed}>
      {items.map((formStep, idx) => (
        <Photo
          onClick={() => dispatch(retakePhoto(formStep.formIndex))}
          key={idx}
          photo={formStep.photo}
          title={filteredSteps[idx]?.helpTitle}
        />
      ))}
    </TabPhotoPanel>
  );
};
