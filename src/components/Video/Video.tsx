import { h } from 'preact';
import styles from './Video.module.scss';
import { forwardRef } from 'preact/compat';
import { useMediaContainerSize } from '../../hooks/mediaDevices';

const CLASS = 'video';

interface Props {
  // mask accept to over video
  mask?: string;
  onClick?: () => void;
}

// eslint-disable-next-line react/display-name
export const Video = forwardRef<HTMLVideoElement, Props>(({ mask = '', onClick }, ref) => {
  const { width, height } = useMediaContainerSize();

  return (
    <div onClick={onClick} style={{ width, height }} className={styles[CLASS]}>
      <video playsInline autoPlay muted className={styles[`${CLASS}__player`]} ref={ref} />
      {mask && <div className={styles[`${CLASS}__mask`]} style={{ backgroundImage: `url(${mask})` }} />}
    </div>
  );
});
