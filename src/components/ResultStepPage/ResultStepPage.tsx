import { FunctionalComponent as FC, h } from 'preact';
import styles from './ResultStepPage.module.scss';
import { TabPhoto, TabPhotoAdditional } from '../TabPhoto';
import { Layout } from '../Layout';
import { Button } from '../Button';
import { getTranslates, groupByPhotoType } from '../../utils/plugin';
import { PluginProps } from '../../types';
import { useCallback, useContext } from 'preact/hooks';
import { FormContext } from '../../reducers/form';
import { useCompleteFormCallback } from '../../hooks/plugin';
import { turnOffUserMedia } from '../../utils/mediaDevices';

const CLASS = 'result-step-page';

interface Props {
  steps: PluginProps['steps'];
  completeFormCallback: PluginProps['completeFormCallback'];
}

export const ResultStepPage: FC<Props> = ({ steps, completeFormCallback }) => {
  const translates = getTranslates();
  const {
    state: { resultForm, videoStream },
  } = useContext(FormContext);
  const formSteps = resultForm.steps;
  const groups = groupByPhotoType(formSteps);
  const completeCallback = useCompleteFormCallback(completeFormCallback);

  const handleSubmit = useCallback(() => {
    turnOffUserMedia(videoStream);

    completeCallback(resultForm);
  }, [completeCallback, resultForm]);

  return (
    <Layout>
      <div className={styles[`${CLASS}__content`]}>
        <p className={styles[`${CLASS}__title`]}>{translates.reviewMediaTitle}</p>

        {Object.keys(groups).map(groupKey => (
          <TabPhoto key={groupKey} title={groupKey} steps={steps} filterKey={groupKey} formSteps={formSteps} />
        ))}

        <TabPhotoAdditional title={translates.additional} />
      </div>

      <div className={styles[`${CLASS}__btn`]}>
        <Button onClick={handleSubmit}>{translates.submitBtn}</Button>
      </div>
    </Layout>
  );
};
