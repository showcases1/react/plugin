import { FunctionalComponent as FC, h } from 'preact';
import styles from './Layout.module.scss';
import { isMobileDevice } from '../../utils/mobile';
import { useOrientation } from '../../hooks/orientation';
import cn from 'classnames';
import { getTranslates } from '../../utils/plugin';

const CLASS = 'layout';

interface Props {
  forceLandscape?: boolean;
}

/**
 * If orientation change app will rerender in app.tsx
 */
export const Layout: FC<Props> = ({ children, forceLandscape = false }) => {
  const { isLandscape } = useOrientation();
  const isMobile = isMobileDevice();
  const translates = getTranslates();

  return (
    <div className={cn(styles[CLASS], { [styles[`${CLASS}--landscape`]]: isLandscape })}>
      {isMobile && isLandscape && !forceLandscape ? (
        <div className={styles[`${CLASS}__orientation`]}>{translates.positionInPortrait}</div>
      ) : isMobile && !isLandscape && forceLandscape ? (
        <div className={styles[`${CLASS}__orientation`]}>{translates.positionInLandscape}</div>
      ) : (
        children
      )}
    </div>
  );
};
