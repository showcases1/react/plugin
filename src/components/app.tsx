import { FunctionalComponent as FC, h } from 'preact';
import { useEffect, useReducer, useState } from 'preact/hooks';
import { PhotoStepsPage } from './PhotoStepsPage';
import styles from './app.module.scss';

import { PermissionsPage } from './PermissionsPage';
import { preload } from '../utils/preload';
import { ResultStepPage } from './ResultStepPage';
import { AdditionalPhotoStep } from './AdditionalPhotoStep';
import { getTestOptions } from '../config/testPluginOptions';
import { RetakePhotoStep } from './RetakePhotoStep';
import { PluginProps, Steps } from '../types';
import { Spinner } from './Spinner';
import { delay } from '../utils/common';
import { getUserMedia, isMediaDeviceAccepted } from '../utils/mediaDevices';
import { isGeolocationAccepted } from '../utils/geolocation';
import { initialState, reducer, FormContext, actions } from '../reducers/form';
import { StepRoute } from './StepRoute';

const { changeStep } = actions;

/**
 * Main component for plugin
 */
const App: FC<PluginProps> = options => {
  options = getTestOptions();

  const [, setForceUpdate] = useState(0);
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    // special force render App on orientationchange to re-render video container size
    const forceRender = () => setTimeout(() => setForceUpdate(count => ++count), 100);

    window.addEventListener('orientationchange', forceRender);
    window.addEventListener('resize', forceRender);

    preload(options.steps?.map(step => [step.maskImg, step.helpImg, step.previewImg]));

    try {
      // preload all steps image
      // TODO: add icons from options
      preload(options.steps?.map(step => [step.maskImg, step.helpImg, step.previewImg]));
    } catch {}

    // get geolocation/user media to determinate page
    delay()
      .then(isGeolocationAccepted)
      .then(isMediaDeviceAccepted)
      .then(getUserMedia)
      .then(() => dispatch(changeStep(Steps.MakePhoto)))
      .catch(() => dispatch(changeStep()));

    return () => {
      window.removeEventListener('orientationchange', forceRender);
      window.removeEventListener('resize', forceRender);
    };
  }, []);

  return (
    <FormContext.Provider value={{ state, dispatch }}>
      <div className={styles.plugin}>
        <StepRoute step={Steps.AwaitMedia}>
          <Spinner />
        </StepRoute>
        <StepRoute step={Steps.EnablePermission}>
          <PermissionsPage />
        </StepRoute>
        <StepRoute step={Steps.MakePhoto}>
          <PhotoStepsPage steps={options.steps} withSummary={options.withSummary} completeFormCallback={options.completeFormCallback} />
        </StepRoute>
        <StepRoute step={Steps.SendForm}>
          <ResultStepPage completeFormCallback={options.completeFormCallback} steps={options.steps} />
        </StepRoute>
        <StepRoute step={Steps.Additional}>
          <AdditionalPhotoStep />
        </StepRoute>
        <StepRoute step={Steps.Retake}>
          <RetakePhotoStep steps={options.steps} />
        </StepRoute>
      </div>
    </FormContext.Provider>
  );
};

export default App;
