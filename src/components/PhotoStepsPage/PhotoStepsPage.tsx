import { FunctionalComponent as FC, h } from 'preact';
import { useCallback, useContext, useState } from 'preact/hooks';
import { CommentPhoto } from './CommentPhoto';
import { Photo } from './Photo';
import { HelpModal } from './HelpModal';
import { FormStep, PluginProps } from '../../types';
import { FormContext, actions } from '../../reducers/form';
import { useCompleteFormCallback } from '../../hooks/plugin';
import { useGeolocation } from '../../hooks/geolocation';
import { turnOffUserMedia } from '../../utils/mediaDevices';

const { changeStep, setResultFormStep } = actions;

interface Props {
  steps: PluginProps['steps'];
  completeFormCallback: PluginProps['completeFormCallback'];
  withSummary?: boolean;
}

// help modal
enum Steps {
  ShowModal,
  MakePhoto,
  CommentPhoto,
}

/**
 * Component for all steps which include photo
 */
export const PhotoStepsPage: FC<Props> = ({ steps, completeFormCallback, withSummary = true }) => {
  const [step, setStep] = useState(0);
  const [photo, setPhoto] = useState('');
  const [comment, setComment] = useState('');
  const [previousPhoto, setPreviousPhoto] = useState(''); // need if retake photo
  const {
    state: { resultForm, videoStream },
    dispatch,
  } = useContext(FormContext);
  const position = useGeolocation(step);

  const isShowComment = steps[step]?.isShowComment;
  const isShowHelpModal = Boolean(steps[step]?.helpImg);
  const withRetake = steps[step].withRetake === undefined ? true : steps[step].withRetake;
  const currentStep = steps[step];
  const nextStep = steps[step + 1];
  const isLastStep = step === steps.length - 1;
  const completeCallback = useCompleteFormCallback(completeFormCallback);
  const [photoState, setPhotoState] = useState<Steps>(isShowHelpModal ? Steps.ShowModal : Steps.MakePhoto);

  const handleNextStep = useCallback(
    (formStep: FormStep) => {
      // reset comment
      setComment('');

      if (isLastStep && !withSummary) {
        turnOffUserMedia(videoStream);
        // merge form result cuz dispatch doesnt update data in hook
        return completeCallback({
          ...resultForm,
          steps: [...resultForm.steps, formStep],
        });
      }

      // TODO: check last step, if blocks were change
      if (isLastStep) {
        // move to summary
        return dispatch(changeStep());
      } else {
        setStep(step => ++step);
      }

      if (nextStep?.helpImg) {
        setPhotoState(Steps.ShowModal);
      } else {
        setPhotoState(Steps.MakePhoto);
      }
    },
    [completeCallback, isLastStep, withSummary, nextStep, resultForm, videoStream]
  );

  const handleRetake = useCallback(() => {
    setPhotoState(Steps.MakePhoto);
    setPhoto(previousPhoto);
  }, [previousPhoto]);

  const handleSave = useCallback(
    async (comment = '', newPhoto?: string) => {
      const date = new Date();

      const formStep = {
        comment,
        photo: newPhoto || photo,
        photoType: currentStep.photoType || '',
        geolocation: {
          date: date.toLocaleDateString(),
          time: date.toLocaleTimeString(),
          latitude: position?.coords.latitude,
          longitude: position?.coords.longitude,
        },
      };

      dispatch(setResultFormStep(formStep));

      handleNextStep(formStep);
    },
    [handleNextStep, photo, position]
  );

  const handlePhoto = useCallback(
    (newPhoto: string, comment: string) => {
      if (withRetake) {
        setPhoto(newPhoto);
        setPreviousPhoto(photo);
        setComment(comment);
        setPhotoState(Steps.CommentPhoto);
      } else {
        // send photo to save form
        handleSave(comment, newPhoto);
      }
    },
    [photo, withRetake, handleSave]
  );

  return photoState === Steps.ShowModal ? (
    <HelpModal
      title={currentStep?.helpTitle}
      img={currentStep?.helpImg}
      description={currentStep?.helpDescription}
      forceLandscape={currentStep?.forceLandscape}
      onClick={() => setPhotoState(Steps.MakePhoto)}
    />
  ) : photoState === Steps.MakePhoto ? (
    <Photo
      comment={comment}
      showComment={isShowComment}
      embedGeo={currentStep?.embedGeo}
      maskImg={currentStep?.maskImg}
      forceLandscape={currentStep?.forceLandscape}
      onPhoto={handlePhoto}
    />
  ) : (
    <CommentPhoto
      comment={comment}
      showComment={isShowComment}
      onSave={handleSave}
      onRetake={handleRetake}
      img={photo}
      slider={{
        current: currentStep?.previewImg,
        currentTitle: currentStep?.previewTitle,
        currentCount: step,
        after: nextStep?.previewImg,
        before: photo,
        count: steps.length,
      }}
      forceLandscape={currentStep?.forceLandscape}
    />
  );
};
