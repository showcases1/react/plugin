import { FunctionalComponent as FC, h } from 'preact';
import { Layout } from '../../Layout';
import styles from './HelpModal.module.scss';
import { useOrientation } from '../../../hooks/orientation';
import { getTranslates } from '../../../utils/plugin';

const CLASS = 'help-modal';

interface Props {
  onClick: () => void;
  img?: string;
  title?: string;
  description?: string;
  forceLandscape?: boolean;
}

export const HelpModal: FC<Props> = ({ onClick, img, title, description, forceLandscape }) => {
  const { isLandscape } = useOrientation();
  const translates = getTranslates();

  return (
    <Layout forceLandscape={forceLandscape}>
      <div
        style={{ justifyContent: isLandscape ? 'flex-start' : 'center', padding: isLandscape ? '30px 0 0' : 0 }}
        onClick={onClick}
        className={styles[CLASS]}
      >
        <img src={img} alt="" />
        <p className={styles[`${CLASS}__title`]}>{title}</p>
        <p className={styles[`${CLASS}__description`]}>{description}</p>
        <p className={styles[`${CLASS}__tap`]}>{translates.tapToDismiss}</p>
        <div className={styles[`${CLASS}__close`]} />
      </div>
    </Layout>
  );
};
