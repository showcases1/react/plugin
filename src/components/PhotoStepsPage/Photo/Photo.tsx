import { FunctionalComponent as FC, h } from 'preact';
import { useCallback, useContext, useEffect, useRef, useState } from 'preact/hooks';
import { acceptWebCamToVideo, captureVideo } from '../../../utils/mediaDevices';

import { Video } from '../../Video';
import { PhotoButton } from '../../PhotoButton';
import { Layout } from '../../Layout';
import { Separator } from '../../Separator';
import styles from './Photo.module.scss';
import { Comment } from '../../Comment';
import { FormContext, actions } from '../../../reducers/form';

const { setVideoStream } = actions;

interface Props {
  onPhoto: (img: string, comment: string) => void;
  comment?: string;
  showComment?: boolean;
  maskImg?: string;
  embedGeo?: boolean;
  forceLandscape?: boolean;
}

const CLASS = 'photo';

export const Photo: FC<Props> = ({ onPhoto, maskImg, embedGeo, forceLandscape, comment: commentProps = '', showComment = true }) => {
  const [renderedVideo, setRenderedVideo] = useState(false);
  const videoRef = useRef<HTMLVideoElement>();
  const [comment, setComment] = useState(commentProps);
  const { dispatch } = useContext(FormContext);

  useEffect(() => {
    const acceptVideo = async () => {
      if (videoRef?.current && !renderedVideo) {
        setRenderedVideo(true);

        const stream = await acceptWebCamToVideo(videoRef.current);

        dispatch(setVideoStream(stream));
      }
    };

    acceptVideo();

    return () => {
      setRenderedVideo(false);
    };
  }, [videoRef.current]);

  const handlePhoto = useCallback(async () => {
    const image = await captureVideo(videoRef.current, embedGeo);

    onPhoto(image, comment);
  }, [onPhoto, embedGeo, comment]);

  return (
    <Layout forceLandscape={forceLandscape}>
      <Video onClick={handlePhoto} mask={maskImg} ref={videoRef} />

      <div className={styles[CLASS]}>
        <Comment showComment={showComment} value={comment} onChange={setComment}>
          <Separator height={20} />
        </Comment>

        <Separator fit />

        <PhotoButton onClick={handlePhoto} />
      </div>
    </Layout>
  );
};
