import { FunctionalComponent as FC, h } from 'preact';
import { Layout } from '../../Layout';
import styles from './CommentPhoto.module.scss';
import { useState } from 'preact/hooks';
import { Separator } from '../../Separator';
import { useMediaContainerSize } from '../../../hooks/mediaDevices';
import { Slider, SliderProps } from '../../Slider';
import { Comment } from '../../Comment';
import { getIcons } from '../../../utils/plugin';

const CLASS = 'comment-photo';

interface Props {
  // link or base64
  img: string;
  onRetake: () => void;
  onSave: (comment: string) => void;
  showComment?: boolean;
  forceLandscape?: boolean;
  comment?: string;
  slider?: SliderProps;
}

export const CommentPhoto: FC<Props> = ({
  img,
  onRetake,
  onSave,
  forceLandscape,
  slider,
  comment: commentProps = '',
  showComment = true,
}) => {
  const [comment, setComment] = useState(commentProps);
  const { width, height } = useMediaContainerSize();
  const { retake, usePhoto } = getIcons();

  return (
    <Layout forceLandscape={forceLandscape}>
      <div className={styles[`${CLASS}__photo-wrapper`]} style={{ width, height }}>
        <img className={styles[`${CLASS}__photo`]} src={img} alt="" />
      </div>

      <div className={styles[`${CLASS}__main`]}>
        <Comment showComment={showComment} value={comment} onChange={setComment}>
          <div>
            {slider && <Slider {...slider} />}

            {slider && <Separator height={110} />}

            {!slider && <Separator height={20} />}
          </div>
        </Comment>

        <div className={styles[`${CLASS}__btn-group`]}>
          <div className={styles[`${CLASS}__btn`]} style={{ backgroundImage: `url(${retake})` }} onClick={onRetake} />

          <Separator width={20} />

          <div className={styles[`${CLASS}__btn`]} style={{ backgroundImage: `url(${usePhoto})` }} onClick={() => onSave(comment)} />
        </div>
      </div>
    </Layout>
  );
};
