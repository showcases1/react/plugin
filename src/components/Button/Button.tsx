import { FunctionalComponent as FC, h } from 'preact';
import styles from './Button.module.scss';
import cn from 'classnames';

const CLASS = 'button';

interface Props {
  onClick?: h.JSX.MouseEventHandler<HTMLButtonElement>;
  type?: 'primary' | 'secondary';
}

export const Button: FC<Props> = ({ children, onClick, type = 'primary' }) => {
  return (
    <button onClick={onClick} className={cn(styles[CLASS], { [`${styles[`${CLASS}__secondary`]}`]: type === 'secondary' })}>
      {children}
    </button>
  );
};
