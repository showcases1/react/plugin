export enum Steps {
  AwaitMedia,
  EnablePermission,
  MakePhoto,
  SendForm,
  Additional,
  Retake,
}

export interface FormStep {
  // base64 string
  photo: string;
  photoType: PluginStep['photoType'];
  comment?: string;
  geolocation?: {
    latitude?: number;
    longitude?: number;
    date: string;
    time: string;
  };
}

export interface ResultForm {
  steps: FormStep[];
  // additional photo
  additionalSteps?: string[];
}

export interface PluginStep {
  // type of photo ('damage' | 'vehicle' or ets.)
  photoType: string;
  // show retake page after capture (default is true)
  withRetake?: boolean;
  // embed geolocation, date, time in the photo (default is true)
  embedGeo?: boolean;
  // show step in landscape mode (default is false it is mean forcePortrait)
  forceLandscape?: boolean;
  // show comment block after make photo
  isShowComment?: boolean;
  // image for help photo modal (if empty "Help modal" doesnt show before capture)
  helpImg?: string;
  // title below help image in modal
  helpTitle?: string;
  // description of help image
  helpDescription?: string;
  // image for mask over video
  maskImg?: string;
  // small image in slider during photo
  previewImg?: string;
  // title for preview image in slider during photo
  previewTitle?: string;
}

export interface PluginProps {
  // selector to element where plugin will render
  selector: string;
  steps: PluginStep[];
  // show summary page (default 'true') if false completeFormCallback invoke in last capture photo step
  withSummary?: boolean;
  completeFormCallback: (form: ResultForm) => void;
  // default true
  disableStreamAfterComplete?: boolean;
  translates?: {
    enablePermissionsTitle?: string;
    enablePermissionsDescription?: string;
    enablePermissionsBtn?: string;
    refreshPageBtn?: string;
    disablePermissionsTitle?: string;
    enablePermissionsSubTitle?: string;
    disablePermissionsSubTitle?: string;
    enableLocationBtn?: string;
    rejectCameraDescription?: string;
    rejectLocationDescription?: string;
    // special text message for Safari
    rejectLocationDescriptionSafari?: string;
    enablePermissionsSkip?: string;
    tapToDismiss?: string;
    commentFieldPlaceholder?: string;
    positionInLandscape?: string;
    positionInPortrait?: string;
    reviewMediaTitle?: string;
    submitBtn?: string;
    additional?: string;
  };
  icons?: {
    permissions?: string;
    rejectPermissions?: string;
    photoBtn?: string;
    retake?: string;
    usePhoto?: string;
    additional?: string;
    spinner?: string;
  };
  gpsInfo?: {
    // default 'top-right'
    position?: 'top-left' | 'top-right' | 'bottom-left' | 'bottom-right' | 'center-top' | 'center-bottom';
    styles?: {
      // only words or rgb (e.g 'white' or 'rgb(255, 255, 10)')
      fontColor?: string;
      // only px (default 25px)
      fontSize?: string;
      // default Arial
      fontFamily?: string;
      // default black
      backgroundColor?: string;
      // only px only 4 values supports (e.g '10px 10px 10px 10px') only integer
      padding?: string;
      // only px
      borderRadius?: string;
    };
  };
}

export type Dictionary<T = any> = {
  [index: string]: T;
};

type DictionaryActions = Dictionary<(...args: any) => any>;

export type ExtractActions<T extends DictionaryActions> = {
  [P in keyof T]: ReturnType<T[P]>;
}[keyof T];
